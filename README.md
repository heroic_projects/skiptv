##Adblock for live IPTV

The revolutionary new adblock solution for live IPTV. 

SKIPTV beta preview app is available to [download](https://bitbucket.org/heroic_projects/skiptv/raw/master/SKIPTV-beta-1.0.4.apk) as a sideloadable APK.

![EPG SCREEN](https://bitbucket.org/heroic_projects/skiptv/raw/master/EPG.png)
*red dot indicates live adblocking available*
---


##Project aim:

To block 100% of live TV ads in realtime.


##Requirements:

1) Android 4.2+ (Android TV app coming soon)

2) Existing IPTV service via .m3u playlist and .xml guide URLs


##Getting started:

1) "Add new playlist" from main menu and paste in your playlist + epg url

2) You must **star at least two favorites** from the "PLAYLISTS" tab

optional: reorganize favorite priority in "SAVED" tab so that an adfree TV channel has top secondary priority. Beta will only check the primary channel for ads.

3) Play any item you wish from the "EPG" tab 


##User Controls (after ad detected):

1) Volume UP - ESCAPE/CANCEL

2) Volume DOWN - EXTEND BLOCKOUT FOR 30s



---


##Notes:

The app is designed to work completely automatically but false positives may occur! in which case please flag them by pressing VOL UP before the channel swap. Patience is required on all new channels. Watching normally will populate the back-end ad catalogue for your current channel, however, it can take a half hour before known ads are seen again and blocked.


##Additional notes: 

Fine user control options via **volume rocker**:


1) Immediately **CANCEL** an upcoming adswap via **VOL UP** button (rare false positive)

3) Immediately **ESCAPE** the current adswap via **VOL UP** button (return to primary channel early)

3) Temporarily **EXTEND** the current adswap via **VOL DOWN** button (continue on the secondary channel) 

4) Temporarily **MUTE** after a prematurely ended adswap via **VOL DOWN** button (primary channel is unfortunately still playing ads after swapping back, this is for when you want to mute for the last little bit)


After an auto-adswap event is complete the VOL keys return to normal function until the next ad is detected.

The default timeout length of 100 seconds is only a guide which can be changed in settings. The app is smart enough to auto adjust this shorter or longer depending on user input and ads detected.

---

Comments/feedback/suggestions are much appreciated as we transition out of the alpha lab and scale up. Goto [AdJammer Website](http://adjammer.surge.sh) for more information.